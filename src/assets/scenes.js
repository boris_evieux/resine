import StartBtn from "../interfaces/StartBtn.html";
import Credits from "../interfaces/Credits.html";
import {V3, V2} from "../math/vectors.js";
import {M4, M3} from "../math/matrices.js";
import intro from "./intro.js";
import data_files from "../assets/assets.js";
export default function(world, engine, game){
	const state = {};

	let scene = play(intro, {});
	const scenes = [scene1,scene2,scene3,scene4,scene5,scene6,scene8,scene9,fin]; //saute des scènes en les commentant
	for(let next of scenes){
		scene = scene.then(_=>play(next, state));
	}


	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	function scene1(state, world, engine, game) {
	  const { backdrop, textures, silhouette, theatre } = state,
	    { renderHeight, renderWidth } = game.get(),
	    musicLoop = engine.playSound("sons/Teddy Bear Waltz",{loop : true, fade : 5, volume : 0.2}).source,
	    knight = createCharacter(
	      textures["images/bonhomme-corps"],
	      textures["images/bonhomme-tete"],
	      textures["images/bonhomme-pied-1"],
	      textures["images/bonhomme-pied-2"],
	      "bonhomme"
	    ),
	    princess = createSprite(
	      textures["images/princesse"],
	      838,
	      287,
	      17,
	      renderWidth,
	      { layer: 3 }
	    ),
	    chateau = createSprite(
	      textures["images/chateau"],
	      630,
	      590,
	      2.59,
	      renderWidth,
	      { layer: 3 }
	    ),
	    grille = createSprite(
	      textures["images/chateau-grilles"],
	      630,
	      590,
	      2.59,
	      renderWidth,
	      { layer: 6 }
	    );

	  setTimeout(_ => {
	    if (silhouette) silhouette.destroy();
	    world.add(knight, null, {layer : 7});
	  	//world.add(theatre, null, {layer : 8});
	  }, 2762);

	  setTimeout(_ => world.add(princess), 4468);
	  setTimeout(_ => world.add(chateau), 5726);
	  setTimeout(_ => world.add(grille), 7251);

	  Object.assign(state, { knight, princess, chateau, grille, musicLoop });
	  return Promise.all([
	  	playAllVoices(state, "02"),
	  	new Promise( (resolve, reject)=>{
	  		world.add(_=>{
	  			if(knight.getPos().x > renderWidth / 3){
	  				resolve();
	  				return {remove : true};
	  			}
	  		});
	  	})
	  ]).then (_=>engine.playSound("sons/win"));
	}

	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */

	function scene2(state, world, engine, game) {
	  const voices = ["03"];
	  const gameplay = new Promise((resolve, reject) => {
	    const { textures } = state,
	      { knight, princess, chateau, grille } = state,
	      { renderHeight, renderWidth } = game.get(),
	      bg = textures["images/mechant"],
	      badGuyPos = [
	        [V2.create(445, 532), 3.57],
	        [V2.create(203, 521), 4.13],
	        [V2.create(730,	521), 4.13],
	        [V2.create(91, 509), 6.15],
	        [V2.create(977, 465), 5.28],
	      ],
	      clicks = Array(badGuyPos.length),
	      badGuys = badGuyPos.map(([pos, ratio], i) => {
	        const guy = createSprite(bg, pos.x, pos.y, ratio, renderWidth,
	          {layer: 3, interactive : true}
	        );
	        world.add(guy);
			guy.click = () => {
				console.log("clicked on ", guy, 1)
				guy.interactive = false;
				guy.click = null;
				moveTo(guy, -500, Math.random * 5000 - 2000, 250, false).then(_ =>
				  guy.destroy()
				);
				clicks[i] = true;
				for(let click of clicks)
					if(!click) return;
				resolve();
			};
	        return guy;
	      });
	    knight.interactive = false;
	    knight.dragging = false;
	    knight._drag = knight.drag;
	    knight.drag =null;
	    moveTo(knight, 200, 100, 1500, false);

	    for (let obj of [princess, chateau, grille]) {
	      moveTo(obj, 250, 0, 1500, true);
	    }

		  setTimeout(_ => {
		    chateau.destroy();
		    grille.destroy();
		    princess.destroy();
		  }, 5000);
	    Object.assign(state, { badGuys });
	  });
	  return Promise.all([playAllVoices(state, ...voices), gameplay]).then(_ =>
	    engine.playSound("sons/win")
	  );
	}

	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */

	function scene3(state, world, engine, game) {
	  const voices = ["04"],
	    { backdrop, textures, chateau, grille, princess } = state, //<= les choses � r�cup�rer des sc�nes pr�c�dentes
	    { renderHeight, renderWidth } = game.get(),
	    desert = textures["images/desert"],
	    pierre = createSprite(
	      textures["images/pierre"],
	      746,
	      261,
	      5.71,
	      renderWidth,
	      { layer: 3 }
	    ),
	    epee = createSprite(textures["images/epee"], 835, 386, 20.88, renderWidth, {
	      layer: 3
	    }),
	    pierreFG = createSprite(
	      textures["images/pierre-premier-plan"],
	      746,
	      261,
	      5.71,
	      renderWidth,
	      { layer: 3 }
	    );

	  //On passe au désert, on vire la princesse
	  setTimeout(_ => {
	    backdrop.setTexture(desert);
	  }, 3578);

	  setTimeout(_ => world.add(epee, world.root, 4), 1977); //<= les timestamps
	  setTimeout(_ => world.add(pierre, world.root, 3), 1977);
	  setTimeout(_ => world.add(pierreFG, world.root, 6), 1977);

	  Object.assign(state, { epee, pierre, pierreFG }); //<= �a passe les choses � la sc�ne suivante
	  return playAllVoices(state, ...voices); //parle et affiche le bouton
	}

	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */

	function scene4(state, world, engine, game) {
	  const voices = ["05"],
	    { backdrop, textures, pierre, pierreFG, epee } = state,
	    { renderHeight, renderWidth } = game.get(),
	    princess = createSprite(
	      textures["images/princesse"],
	      696,
	      348,
	      11.13,
	      renderWidth,
	      { layer: 3 }
	    ),
	    barriere = createSprite(
	      textures["images/barriere"],
	      380,
	      523,
	      2.4,
	      renderWidth,
	      { layer: 3 }
	    ),
	    epee_140 = createSprite(
	      textures["images/epee-140"],
	      459,
	      327,
	      7.52,
	      renderWidth,
	      { layer: 3 }
	    );

	  //Cimetière

	  setTimeout(_ => {
	    pierre.destroy();
	    pierreFG.destroy();
	    epee.destroy();
	  }, 3610);

	  setTimeout(_ => {
	    backdrop.setTexture(textures["images/cimetiere"]);
	  }, 4239);

	  setTimeout(_ => world.add(epee_140, world.root, 4), 3610);
	  setTimeout(_ => world.add(barriere, world.root, 4), 3610);
	  setTimeout(_ => world.add(princess, world.root, 4), 5990);
	  Object.assign(state, { barriere, princess, epee_140 });
	  return playAllVoices(state, ...voices);
	}

	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */

	function scene5(state, world, engine, game) {
	  const voices = ["06"],
	    { backdrop, textures, barriere, princess, epee_140 } = state, //<= les choses � r�cup�rer des sc�nes pr�c�dentes
	    { renderHeight, renderWidth } = game.get(),
	    echelle = createSprite(
	      textures["images/echelle"],
	      674,
	      606,
	      8.42,
	      renderWidth,
	      { layer: 3 }
	    );

	  setTimeout(_ => {
	    princess.destroy();
	  }, 3166);


	  setTimeout(_ => world.add(echelle, world.root, 4), 3170);
	  Object.assign(state, { barriere, echelle });
	  return Promise.all([
	  	playAllVoices(state, ...voices),
	  	moveTo(epee_140, 100,400,400)
	  		.then (_=> new Promise( (resolve, reject)=>{
	  			  		let dragStartPos;
	  			  		epee_140.interactive = true;
	  			  		epee_140.draggable = true;
	  			  		epee_140.drag = (start, stop, ongoing, localPos, globalPos)=>{
	  			  			const norm = V2.norm(localPos);
	  			  			if(start) dragStartPos = localPos;
	  			  			if(stop){
	  			  				dragStartPos = null;
	  			  			} 
	  			  			if(dragStartPos){
	  			  				const newPos = V2.sub(globalPos, dragStartPos);
	  			  				epee_140.setPos(newPos);
	  			  				return true;
	  			  			}
	  			  			return false;
	  			  		};
	  			  		world.add(_=>{
	  			  			if(epee_140.getPos().x >  2 * renderWidth / 3){
			  			  		epee_140.interactive = false;
			  			  		epee_140.draggable = false;
			  			  		barriere.destroy();
	  							moveTo(epee_140, 100,400,1000)
	  			  				resolve();
	  			  				return {remove : true};
	  			  			}
	  			  		})
	  			  	}))
	  ]).then (_=>engine.playSound("sons/win"));
	  return playAllVoicesAndWin(state, ...voices);
	}

	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */

	function scene6(state, world, engine, game) {
		let all_pieces = false,
			 all_guys = false;
	  const voices = ["07"],
	    { backdrop, textures, barriere, echelle, knight } = state, //<= les choses � r�cup�rer des sc�nes pr�c�dentes
	    { renderHeight, renderWidth } = game.get(),
	    bg = textures["images/piece"],
	    piecesPos = [
	      [V2.create(573, 740), 22.89],
	      [V2.create(568, 199), 47.23],
	      [V2.create(340, 551), 64],
	      [V2.create(480, 543), 28.44],
	      [V2.create(750, 558), 28.44],
	      [V2.create(505, 337), 22.89],
	      [V2.create(1072, 402), 64],
	      [V2.create(430, 288), 64],
	      [V2.create(251, 458), 22.89],
	      [V2.create(641, 371), 47.23],
	      [V2.create(1061, 502), 47.23],
	      [V2.create(909, 322), 22.89],
	      [V2.create(1117, 88), 47.23],
	      [V2.create(640, 185), 28.44],
	      [V2.create(354, 80), 47.23],
	      [V2.create(314, 186), 22.89],
	      [V2.create(1048, 275), 28.44],
	      [V2.create(656, 481), 64],
	      [V2.create(880, 140), 64],
	      [V2.create(102, 174), 22.89]
	    ],
	    piece_magique = createSprite(
	      textures["images/piece-magique"],
	      674,
	      606,
	      8.42,
	      renderWidth,
	      { layer: 3 }
	    ),
	    clicks_pieces = piecesPos.map(_=>false),
	    pieces = piecesPos.map(([pos, scale], i) => {
	      const piece = createSprite(bg, pos.x, pos.y, scale, renderWidth, {
	      	interactive : true,
	        layer: 3
	      });
	      piece.hover = () => {
				
				piece.interactive = false;
				piece.click = null;
				moveTo(piece, renderWidth + 500, 0, 250, false).then(_ =>
				  piece.destroy()
				);
				clicks_pieces[i] = true;
				const clicked = clicks_pieces.reduce((a, c)=>a+ (c?1 :0), 0);
				console.log(clicked, clicks_pieces);
				if(clicked < piecesPos.length ) return;
				all_pieces = true;
				const pos = knight && knight.getPos() || {x : 100, y:100};
				moveTo(piece_magique, 400, 500, 350);
				engine.playSound("sons/win");
			}
	      world.add(piece, null, 4);
	      return piece;
	    }),
	      cartePos = [
	        [V2.create(445, 532), 3.57],
	        [V2.create(203, 521), 4.13],
	        [V2.create(91, 509), 6.15],
	        [V2.create(730,	521), 4.13],
	        [V2.create(977, 465), 5.28],
	      ],
	      clicks = cartePos.map(_=>false),
	      cartes = cartePos.map(([pos, ratio], i) => {
	        const guy = createSprite(textures["images/carte-a-jouer"], pos.x, pos.y, ratio, renderWidth,
	          {layer: 3, interactive : true}
	        );
	        //world.add(guy);
			guy.click = () => {
				if(!all_pieces)return;
				guy.interactive = false;
				guy.click = null;
				moveTo(guy, -500, Math.random * 5000 - 2000, 250, false).then(_ =>
				  guy.destroy()
				);
				clicks[i] = true;
				for(let click of clicks)
					if(!click) return;
				all_guys = true;
			};
	        return guy;
	      });


	  Object.assign(state, { cartes });
	  setTimeout(_ => {
	    barriere && barriere.destroy();
	    echelle && echelle.destroy();
	  }, 2349);

	  //A FAIRE
	  //Quand on a cliqué sur les 20 pièces, pièce magique apparait, puis les cartes
	  /*****||||||||||||             ************ */
	  /*****||||||||||||             ************ */
	  /*****||||||||||||             ************ */
	  /*****||||||||||||             ************ */

	  setTimeout(_ => world.add(piece_magique, world.root, 4), 5976);
	  let img_pause = engine.createSprite(
	    textures["images/pause"],
	    renderWidth,
	    { width : 280, x :500, y : 500}
	  );
	  return Promise.all([
	  	playAllVoices(state, ...voices)
	  		.then( _=>{
	  			world.add(img_pause)
	  			return engine.playSound("sons/scratch", {volume : 2.0})
	  		})
	  		.then(_=>{
	  			engine.stop();
	  			return playAllVoices(state, "08")
	  		})
	  		.then(_=>{
	  			engine.start({resume : true});
	  			img_pause.destroy();
	  			cartes.forEach(c=>world.add(c, null, 3));
			}),
	  	new Promise( (resolve, reject)=>{
	  		world.add(_=>{
	  			if(all_guys){
	  				resolve();
	  				return {remove : true};
	  			}
	  		})
	  	})
	  ]).then (_=>engine.playSound("sons/win"));

	}

	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */


	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	function scene8(state, world, engine, game) {
	  const voices = ["09"],
	    { backdrop, textures, piece_magique, carte } = state, //<= les choses � r�cup�rer des sc�nes pr�c�dentes
	    { renderHeight, renderWidth } = game.get(),
	    tour = createSprite(textures["images/tour"], 700, 663, 5.59, renderWidth, {
	      layer: 3
	    }),
	  princess = createSprite(
	    textures["images/princesse"],
	    745,
	    582,
	    11.22,
	    renderWidth,
	    { layer: 3 }
	  ),
	  tour_premier_plan = createSprite(
	      textures["images/tour-premier-plan"],
	      700,
	      663,
	      5.59,
	      renderWidth,
	      { layer: 3 }
	    ),
	    echelle = createSprite(
	      textures["images/echelle"],
	      655,
	      476,
	      9.77,
	      renderWidth,
	      { layer: 3 }
	    );

	  setTimeout(_ => {
	    piece_magique && piece_magique.destroy();
	    carte && carte.destroy();
	    backdrop && backdrop.setTexture(textures["images/colline"]);
	  }, 2538);

	  setTimeout(_ => world.add(echelle, world.root, 7), 2538);
	  setTimeout(_ => world.add(tour_premier_plan, world.root, 5), 2538);
	  setTimeout(_ => world.add(princess, world.root, 6), 3656);
	  setTimeout(_ => world.add(tour, world.root, 4), 2538);

	  Object.assign(state, { tour, tour_premier_plan, echelle, princess });

	  return playAllVoices(state, ...voices);
	}

	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */

	function scene9(state, world, engine, game) {
	  const voices = ["10"],
	    { backdrop, textures, tour, tour_premier_plan, echelle, princess, knight } = state, //<= les choses � r�cup�rer des sc�nes pr�c�dentes
	    { renderHeight, renderWidth } = game.get(),
	    parachute = createSprite(
	      textures["images/parachute"],
	      595,
	      690,
	      5.12,
	      renderWidth,
	      { layer: 3 }
	    );
	  setTimeout(_ => {
	    echelle.destroy();
	  }, 1159);


	  moveTo(knight, 130, 120, 1000, false).then(_=>{ knight.interactive = true, knight.drag = knight._drag});
	  setTimeout(_ => world.add(parachute, world.root, 7), 1159);
	  return Promise.all([
	  	playAllVoices(state, ...voices),
	  	new Promise( (resolve, reject)=>{
	  		world.add(_=>{
	  			if(knight.getPos().x > renderWidth / 2){
	  				resolve();
	  				return {remove : true};
	  			}
	  		})
	  	}).then(_=>{
	  		knight.interactive = false;
	  		knight.dragging = false;
	  		//world.add(parachute, world.root, 4);
	  		return moveTo(knight, princess.getPos().x, princess.getPos().y - 120, 800, false)
	  			.then(_=>Promise.all([
	  				moveTo(knight, 800, 120, 2500, true),
	  				moveTo(princess, 800, 120, 2500, true),
	  				moveTo(parachute, 800, 120, 2500, true),
	  			]));
	  	})
	  ]).then (_=>engine.playSound("sons/win"));
	}
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	/******************************************** */
	function fin(state, world, engine, game) {
	  return new Promise((resolve, reject) => {
	    game.playVoice("voix/11");
	    game.addInterface(Credits, {});
	    console.log('on entre dans la scene "fin"');
	  });
	}



function play(scene){
	game.setScene(scene.name);
	return scene(state, world, engine, game);
}
function createSprite(texture, x, y, ratio, renderWidth, options = {}){
	const newy = getY(texture, y, ratio, renderWidth);
	options.x = x;
	options.y = newy;
	options.w = renderWidth / ratio;
	return engine.createSprite(texture, options);
};
function getY(texture, y, ratio, renderWidth){
	const h = texture.height,
	w = texture.width;
	return y-  (renderWidth / ratio) * h / w;
}

	/*debug ! 
	const debugState = {},
	      debugLoad = engine.loadAssets(data_files)
		.then(({textures, sounds})=>{
			Object.assign(debugState, {textures, sound});
			return debugState;
					});
	let scene = debugLoad.then(_=>play(scene1, debugState));
	/*/
	//*/

	function playAllVoicesAndWin(state, ...voices){
		return playAllVoices(state, ...voices).then(_=>new Promise( (clicked, failed)=>{
			setTimeout(
				_=>{
					const data = {
						text : "Win this round",
						action : (event, btn)=>{
							btn.destroy();
							engine.playSound("sons/win");
							clicked(state);
						}
					},
					btn = game.addInterface(StartBtn, data);
				},1500
				)
		}));
	}
	function playAllVoices(state, ...voices){
		function play(name){
			return game.playVoice("voix/"+name).then(state);
		}
		let chain = Promise.resolve(state);
		for(let next of voices){
			chain = chain.then(_=>play(next));
		}
		return chain;
	}
	function moveTo(sprite, destinationX, destinationY, duration, relative){
		const p = new Promise( (resolve, reject) => {
			const   startTime = engine.getTime(),
			start = sprite.getPos(),
			destination = 
			!relative ? V2.create(destinationX, destinationY)
			: V2.add(start, V2.create(destinationX, destinationY)),
			movement = 
			!relative? V2.sub(start, destination)
			: V2.create(destinationX, destinationY),
			theAnim = world.add(time=>{
				const dt = (time - startTime) / duration;
				if(dt > 1){
					sprite.setPos(destination);
					theAnim.destroy();
					world.remove(theAnim, sprite);
					resolve();
				}else
				sprite.setPos(V2.lerp(start, destination, dt));
			}, sprite);
		});
		return p;
	}
	function createCharacter(torsoT,headT,footLT,footRT, name){
		const 	{renderHeight} = game.get(),
		char = engine.createGameobject(world, {name,interactive : true}),
		charPos = V2.create(200, 100),
		charH = renderHeight /3,
		headH =  charH / 3,
		footLH = charH / 6,
		footRH = charH / 6,
		torsoH = charH - headH - footLH - footRH,
		torsoW = torsoH * torsoT.width / torsoT.height,
		headW = headH * headT.width / headT.height,
		footLW = footLH * footLT.width / footLT.height,
		footRW = footRH * footRT.width / footRT.height,
		torsoPos = V2.create(footLW/5, footRH),
		torso = world.add(engine.createSprite( torsoT, {name : "torso", h: torsoH, draggable:true, x:charPos.x + torsoPos.x, y:charPos.y + torsoPos.y, interactive : true} ), char),
		headPos = V2.create(footRH/2, footRH+torsoH),
		head = world.add(engine.createSprite( headT, {name : "head", h: headH, x:charPos.x + headPos.x, y:charPos.y + headPos.y, interactive : false} ), char),
		footLPos = V2.create(0, 0),
		footL = world.add(engine.createSprite( footLT, {name : "footL", h: footLH, x:charPos.x + footLPos.x, y:charPos.y + footLPos.y, interactive : false} ), char),
		footRPos = V2.create(footLW, 0),
		footR = world.add(engine.createSprite( footRT, {name : "footR", h: footRH, x:charPos.x + footRPos.x, y:charPos.y + footRPos.y, interactive : false} ), char),
		parts = [torso, head, footL, footR],
		followparts = [head, footL, footR];
		torso.localPos = torsoPos;
		head.localPos = headPos;
		footL.localPos = footLPos;
		footR.localPos = footRPos;
		char.interactive = true;
		let dragStartPos = null;

		torso.drag = (start, stop, ongoing, localPos, globalPos)=>{
			const norm = V2.norm(localPos);
			if(norm > 350)debugger;
			if(start) dragStartPos = localPos;
			if(stop){
				dragStartPos = null;
			} 
			if(dragStartPos){
				//console.log(" dragging rideauDroit ", start, stop, ongoing, localPos, globalPos);
				const newPos = V2.sub(globalPos, dragStartPos);
				//newPos.y = 0;
				/*if(newPos.x < rightStartX)
					newPos.x = rightStartX;
				if(newPos.x > rightTargetX){
					openedD = true;
					torso.interactive = false;
					if(openedG)doneTwoCurtains()
				}*/
				//console.log(" new position : ", newPos);
				torso.setPos(newPos);
				charPos.x = newPos.x - torsoPos.x;
				charPos.y = newPos.y - torsoPos.y;
				//console.log("new knight pos", charPos);
				return char.interactive && torso.interactive;
			}
			return false;
		};
		char.setPos = function(pos){
			charPos.x = pos.x;
			charPos.y = pos.y;
		}
		char.getPos = function(){
			return V2.create(
				charPos.x, charPos.y
			);
		}
		const threshold = 100;//10 px
		world.add(time=>{//a little sway
			const swaysize = 3,
			swayloopTime = 1250,
			scaledTime = Math.PI * 2 * time / swayloopTime,
			sway = V2.scale(V2.create(Math.cos(scaledTime), Math.sin(scaledTime * 2)/4), swaysize);
			//console.log ("swaying", sway);
			//torso.setRotation(0.1 * sway.x);
			torso.localPos = V2.add(sway, torsoPos);
			torso.setPos(V2.add(charPos, torso.localPos));
			torso.interactive = char.interactive;
		}, torso);
		for(let part of followparts){
			world.add(_=>{//reposition every frame
				let curPos = part.getPos(),
				myDeviation = V2.sub(V2.add(part.localPos, charPos), curPos),
				norm2 = V2.norm2(myDeviation);
				 //console.log ("deviation for", part.name, ":", norm2)
				 if(norm2 > threshold){
				 	let movement = V2.scale(myDeviation, .2 + Math.random() * .2);
				 	part.setPos(V2.add(curPos, movement));
				 }
				//part.setPos(V2.add(charPos, part.localPos));
			}, part)
		}
		Object.assign(char, {torso, head, footL, footR})
		return char;
	}
}


