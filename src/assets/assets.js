export default {
	sounds : [
		"voix/00",
		"voix/01",
		"voix/02",
		"voix/03",
		"voix/04",
		"voix/05",
		"voix/06",
		"voix/07",
		"voix/08",
		"voix/09",
		"voix/10",
		"voix/11",
		//"sons/Hall of the Mountain King"
		"sons/Teddy Bear Waltz",
		"sons/win",
		"sons/scratch",
		"sons/fight",
	],
	images : [
		//"images/emeraude",
		"images/placeholder",
		"images/sol_theatre",
		"images/carte-a-jouer",
		"images/theatre",
		"images/desert",
		"images/bonhomme-tete",
		"images/bonhomme-corps",
		"images/bonhomme-pied-1",
		"images/bonhomme-pied-2",
		"images/colline",
		"images/rideau-2",
		"images/chateau",
		"images/chateau-grilles",
		"images/princesse",
		"images/rideau-1",
		"images/pierre",
		"images/epee",
		"images/epee-140",
		"images/mechant",
		"images/parachute",
		"images/barriere",
		"images/tour",
		"images/tour-premier-plan",
		"images/silhouette",
		"images/echelle",
		"images/pierre-premier-plan",
		"images/cimetiere",
		"images/barriere",
		"images/piece-magique",
		"images/piece",
		"images/pause",
	]
}