import StartBtn from "../interfaces/StartBtn.html";
import TitleCard from "../interfaces/TitleCard.html";
import AssetLoader from "../interfaces/AssetLoader.html";
import data_files from "../assets/assets.js";
import {V3, V2} from "../math/vectors.js";
import {M4, M3} from "../math/matrices.js";
export default function intro(state = {}, world, engine, game){
		console.log("on entre dans la scene \"intro\"");

		const 	canvas = game.refs.canvas,
				{renderWidth, renderHeight} = game.get(),
				leftStartX = 0,
				rightStartX = 200,
				leftTargetX = - renderWidth /4,
				rightTargetX = rightStartX + renderWidth / 4;
		let loader,
			title;
		return  Promise.all([engine.loadAssets({
							sounds : ["voix/00", "voix/01"],
							images : ["images/sol_theatre",
										"images/colline",
										"images/rideau-1",
										"images/rideau-2",
										"images/theatre"]
						}).then(things=>{title.set({go : true});return things}),
					new Promise( (clicked, failed)=>{
										const data = {
											text : "Start the game",
											action : (event, btn)=>{
												//btn.destroy();
												//engine.playSound("sons/win");
												clicked(state);
											}
										};
										title = game.addInterface(TitleCard, data);
									})
					]).then(([{textures, sounds}])=>{
					let dragStartPos, rideauDroit, rideauGauche, openedD = false, openedG = false;
					let sndpromise = Promise.resolve(true).then(_=>{
							return game.playVoice("voix/00", 
							{
								offset: 0.0,
								volume : 1.0,

								//duration : 2
							});
						}).then(_=>{
							rideauDroit.interactive = true;
							rideauGauche.interactive = true;
							return game.playVoice("voix/01", 
							{
								offset: 0.0,
								volume : 1.0,
								//duration : 2
							})
						}).then(_=>{
							console.log("ok, done");
							return true;
						});
					const 	ph = textures["images/placeholder"],
							backdrop = world.add(
								engine.createSprite(
										textures["images/colline"],
										{w : renderWidth, x : 0, h: renderHeight, name: "backdrop"})),
							sol = world.add(engine.createSprite(
								textures["images/sol_theatre"],
								{w : renderWidth, x : 0, h: renderHeight})),
							openRideaux = new Promise( (doneTwoCurtains, fail) =>{
								rideauDroit = world.add(engine.createSprite(textures["images/rideau-2"],
									{name: "rideauDroit",
										interactive: false, draggable: true,
										drag: (start, stop, ongoing, localPos, globalPos)=>{
											if(start) dragStartPos = localPos;
											if(stop) dragStartPos = null;
											if(dragStartPos){
												//console.log(" dragging rideauDroit ", start, stop, ongoing, localPos, globalPos);
												const newPos = V2.sub(globalPos, dragStartPos);
												newPos.y = 0;
												if(newPos.x < rightStartX)
													newPos.x = rightStartX;
												if(newPos.x > rightTargetX){
													openedD = true;
													//rideauDroit.interactive = false;
													if(openedG){
														doneTwoCurtains();
													}
													return false;
												}
												//console.log(" new position : ", newPos);
												rideauDroit.setPos(newPos);
												return true;
											}
										},
										x : rightStartX, h: renderHeight}));
								rideauGauche = world.add(engine.createSprite(textures["images/rideau-1"],
									{name: "rideauGauche",
										interactive: false, draggable: true,
										drag: (start, stop, ongoing, localPos, globalPos)=>{
											if(start) dragStartPos = localPos;
											if(stop) dragStartPos = null;
											if(dragStartPos){
												//console.log(" dragging rideauGauche ", start, stop, ongoing, localPos, globalPos);
												const newPos = V2.sub(globalPos, dragStartPos);
												newPos.y = 0;
												if(newPos.x > leftStartX)
													newPos.x = leftStartX
												if(newPos.x < leftTargetX){
													console.log("rideau-left : done")
													openedG = true;
													//rideauGauche.interactive = false;
													if(openedD) {
														doneTwoCurtains();
													}
													return false;
												}
												//console.log(" new position : ", newPos);
												rideauGauche.setPos(newPos);
												return true;
											}
										},
										x : leftStartX, h: renderHeight}));
							}),
							theatre = world.add(engine.createSprite(
								textures["images/theatre"],
								{w : renderWidth, x : 0, interactive: true, h: renderHeight}))/*,
							animDroite = world.add((time, inputState, parent, world)=>{
								parent.setSize(renderWidth / 2, renderHeight);
								parent.setPos(0, 0);
							}, rideauDroit),
							animGauche = world.add((time, inputState, parent, world)=>{
								parent.setSize(renderWidth / 2, renderHeight);
								parent.setPos(renderWidth / 2, 0);
							}, rideauGauche)*/;//TODO : allow resize
					Object.assign(state, {
						rideauDroit,
						rideauGauche,
						sol,
						backdrop,
						textures,
						theatre
					})
					const loadPromise = engine.loadAssets(data_files)
						.then(_=>{
							const textures = engine.renderer.textures;
							//state.silhouette = world.add(engine.createSprite(textures["images/placeholder"],
							state.silhouette = world.add(engine.createSprite(textures["images/silhouette"],
									{
										h : renderHeight / 3,
										layer : 3,
										x : 200,
										y : 100
									}));
						}).catch(e=>{console.log(e);debugger;});
					return Promise.all([
						sndpromise
						.then(played=> (resolve, reject)=>{
							loader = game.addInterface(AssetLoader);
							return played;
						}),
						loadPromise,
					]).then(_=>openRideaux)
					.then(_=>state);
				}).then(state=>{
					engine.playSound("sons/win");
					const g = state.rideauGauche,
						  d = state.rideauDroit,
						  gp = g.getPos(),
						  start = engine.getTime(),
						  animDuration = 500.0,
						  dp = d.getPos();
					//delete state.rideauGauche;
					//delete state.rideauDroit;
					return Promise.all([
						new Promise((endAnim, failAnim)=>{
								d.interactive = d.dragging = false;
								world.add( (time)=>{
								const   dt = (time - start) / animDuration,
										newPos = V2.lerp(gp, V2.create(-3*renderWidth/4,0), dt);
								g.setPos(newPos);
								if(dt>1.0){
									endAnim();
									return {remove : true};
								}
							}, g)
						}),
						new Promise((endAnim, failAnim)=>{
								d.interactive = d.dragging = false;
								world.add( (time)=>{
								const   dt = (time - start) / animDuration,
										newPos = V2.lerp(dp, V2.create(3*renderWidth/4,0), dt);
								d.setPos(newPos);
								if(dt>1.0){
									endAnim();
									return {remove : true};
								}
							}, d)
						})
					]).then(_=>state);
				});
	}