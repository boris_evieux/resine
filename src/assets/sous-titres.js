export default{
	"fr" : {
		"voix/00" : "Attends! attends! Attends, attends, attends!.. Je crois que c'est bon, ça marche! Allez vas-y c'est à toi!",
		"voix/01" : "En fait, mon histoire c'est... Quelqu'un qui veut aller cherch...Qui... Heuu..",
		"voix/02" : "Ah! Un bonhomme rectangulaire tiens! qui doit aller sauver une princesse dans un château de méchants!",
		"voix/03" : "Mais il doit aller.. il doit aller affronter des méchants moitié métalliques, moitié humains, ",
		"voix/04" : "mais il doit aller chercher une épée magique en plein milieu du désert!",
		"voix/05" : "pour couper une... une barrière hantée qui fait *remprisonnier* la princessse",
		"voix/06" : "et il faut récupérer l'échelle magique incassable! ",
		"voix/07" : "et il faut vingt pièces, pour rassembler tous les morceaux de la pièce unique immm...battable pour vaincre les méchants moitié hommes, moitié humains!",
		"voix/08" : "Attends, t'es sûr? Moitié hommes, moitié humains? ... Bah oui. ... Ok!",
		"voix/09" : "Et il faut grimper au dernier étage de la tour pour sauver la princesse!",
		"voix/10" : "Et il faut un parachute magique pour faire descendre en douceur...!",
		"voix/11" : "Fini!!"
	},
	"en" : {
		"voix/00" : "Mommy : Wait! Waitwaitwait. Wait... I think it's working... yep! Ok go, your turn!",
		"voix/01" : "Kiddo : Ok then, my story is about... <i>someone</i>... who wants to go get... Errr...",
		"voix/02" : "Ah! I know : A rectangular character. Who must go save the princess in a bad guys' castle.",
		"voix/03" : "But he has to go fight a bunch of half-human, half-metallic bad guys.",
		"voix/04" : "But he needs to go find a magic sword in the middle of the desert!",
		"voix/05" : "So he can cut a... a  <span class='b'>HAUNTED</span class='b'>  barrier that re-prisons the princess.",
		"voix/06" : "Then you have to get the unbreakable ladder",
		"voix/07" : "AND to get 20 coins, to reassemble all the pieces of the unique,<br />un...beatable coin, to beat the bad, half-man, half-human people.",
		"voix/08" : "Daddy : wait, you sure ? Half-man, half-human? ... Kiddo : Of course. ... Daddy: Ok!",
		"voix/09" : "Then you have to get to the last floor of the tower to save the princess.",
		"voix/10" : "And you need the magic parachute to go down safely ...!",
		"voix/11" : "The End!!"
	}
};