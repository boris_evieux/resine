attribute vec2 position;
attribute vec2 textureCoord;

uniform mat3 modelMatrix;
//todo : matrix for the sprite
uniform mat4 viewportMatrix;//x,y,width, height

uniform int layer;
uniform int max_layer;

varying highp vec2 texCoord;

void main(void) {
	float z = float(layer / max_layer);
	vec4 pos = vec4( (modelMatrix * vec3(position, 1.0)).xy, layer, 1.0);

	//pos = pos * 2.0 - 1.0;
	gl_Position = (viewportMatrix) * pos;
	texCoord = textureCoord;
}