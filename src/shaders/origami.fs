precision lowp float;
uniform vec2 viewportSize;

varying vec3 edge;
const float EDGE_TH = 0.996;
const float HATCH_TH = 0.98;

void main() {
	vec4 paperColor = mix(vec4(0.7), vec4(0.8,0.2,0.2,1.0), float(gl_FrontFacing));
	float distToEdge = max (max (edge.x, edge.y), edge.z);
	float value = smoothstep(EDGE_TH, EDGE_TH+0.0005,distToEdge);
	vec4 edgeColor = mix(vec4(0.8, 0.8, 0.8, 1.0), vec4(0.8,0.2,0.2,1.0), float(!gl_FrontFacing));
	vec4 hatchColor = mix(vec4(0.8, 0.8, 0.8, 1.0), vec4(0.8,0.2,0.2,1.0), float(!gl_FrontFacing));
	vec4 color = mix(paperColor, edgeColor, value);

	//TODO : understand why the fuck I need to take viewportSize.yy to get proper diagonal hashing
	//redo this when I get it
	vec2 pos = gl_FragCoord.xy * viewportSize.xy; 
	value = smoothstep(HATCH_TH, EDGE_TH ,distToEdge);
	value *= 1.0 - sign(mod(float( int(pos.x + pos.y) ), 6.0));
	gl_FragColor = mix(color, hatchColor, value);
}
