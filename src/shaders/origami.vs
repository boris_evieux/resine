attribute vec4 vertexPosition;
attribute vec3 edgePosition;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

varying vec3 edge;

void main() {
	edge = edgePosition;
	gl_Position = projectionMatrix * modelViewMatrix * vertexPosition;
}