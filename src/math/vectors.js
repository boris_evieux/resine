export const V2 = {
	create : (x, y)=>{
		return {x:x||0.,y:y||0.};
	},
	clone : (v)=>{
		return V2.create(v.x||0., v.y||0.)
	},
	norm2 : (v)=>{
		return v.x*v.x + v.y*v.y;
	},
	norm : (v)=>{
		return Math.sqrt(V2.norm2(v));
	},
	normalized : (v)=>{
	/* Warning, zero vectors will throw*/
		const ni = 1/V2.norm(v);
		return V2.create(v.x*ni, v.y*ni);
	},
	scaled : (v, s)=>{
		return V2.create(s*v.x, s*v.y);
	},
	normalize : (v)=>{
	/* Warning, zero vectors will throw*/
		const ni = 1/V2.norm(v);
		v.x *= n;
		v.y *= n;
		return v;
	},
	scale : (v, s)=>{
		v.x *= s;
		v.y *= s;
		return v;
	},
	dot : (v, w)=>{
		return v.x*w.x + v.y*w.y;
	},
	sub : (a, b)=>{
		return V2.create(a.x - b.x, a.y - b.y);
	},
	add : (a, b)=>{
		return V2.create(a.x + b.x, a.y + b.y);
	},
	lerp : (a, b, t)=>{
		return V2.add(a, V2.scale(V2.sub(b, a), t));
	},
	asArray : (v)=>{
		return [v.x, v.y];
	},
}
export const V3 = {
	create : (x, y, z)=>{
		return {x:x||0.,y:y||0.,z:z||0.};
	},
	clone : (v)=>{
		return V3.create(v.x, v.y, v.z)
	},
	norm2 : (v)=>{
		return v.x*v.x + v.y*v.y + v.z*v.z;
	},
	norm : (v)=>{
		return Math.sqrt(V3.norm2(v));
	},
	/* Warning, zero vectors will throw*/
	normalized : (v)=>{
		const ni = 1/V3.norm(v);
		return V3.create(v.x*ni, v.y*ni, v.z*ni);
	},
	scaled : (v, s)=>{
		return V3.create(s*v.x, s*v.y, s*v.z);
	},
	/* Warning, zero vectors will throw*/
	normalize : (v)=>{
		const ni = 1/V3.norm(v);
		v.x *= n;
		v.y *= n;
		v.z *= n;
		return v;
	},
	scale : (v, s)=>{
		v.x *= s;
		v.y *= s;
		v.z *= s;
		return v;
	},
	dot : (v, w)=>{
		return v.x*w.x + v.y*w.y + v.z*w.z;
	},
	cross : (v, w)=>{
		return V3.create(
			v.y*w.z - v.z*w.y,
			v.z*w.x - v.x*w.z,
			v.x*w.y - v.y*w.x
		);
	},
	sub : (a, b)=>{
		return V3.create(a.x - b.x, a.y - b.y, a.z - b.z);
	},
	add : (a, b)=>{
		return V3.create(a.x + b.x, a.y + b.y, a.z + b.z);
	},
	lerp : (a, b, t)=>{
		return V3.add(a, V3.scale(V3.sub(b, a), t));
	},
	asArray : (v)=>{
		return [v.x, v.y, v.z];
	},
}
export default {V2, V3};