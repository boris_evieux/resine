import{V2, V3} from "./vectors.js";
export const M3 = {
	create : (other)=>{
		const ret = other ? Float32Array.from(other) : new Float32Array(9);
		return ret;
	},
	identity : (toTransform)=>{
		const ret = toTransform || M3.create();
		ret[ 0]=1.; ret[ 1]=0.; ret[ 2]=0.;
		ret[ 3]=0.; ret[ 4]=1.; ret[ 5]=0.;
		ret[ 6]=0.; ret[ 7]=0.; ret[ 8]=1.;
		return ret;
	},
	translate : (m3, v2)=>{
	    m3[6] += m3[0] * v2.x + m3[3] * v2.y
	    m3[7] += m3[1] * v2.x + m3[4] * v2.y
	    m3[8] += m3[2] * v2.x + m3[5] * v2.y
	    return m3;
	},
	scale : (m3, v2)=>{
	    m3[0] *= v2.x
	    m3[4] *= v2.y
	    return m3;
	},
	getTranslation : (m3)=>{
		return V2.create(m3[6],m3[7])
	},
	setTranslation : (m3, v2)=>{
	    m3[6] = v2.x;
	    m3[7] = v2.y;
	    return m3;
	},
	setRotation : (m3, angle)=>{
		const sin = Math.sin(angle),
			  cos = Math.cos(angle);
	    m3[0] = cos;	m3[1] = sin;
	    m3[3] = -sin;	m3[4] = cos;
	    return m3;
	},
	transform : (v, m3)=>{
		const {x = 0, y = 0, z = 0} = v;
	  	return V3.create(
	  			m3[0] * x + m3[3] * y + m3[6] * z,
	  			m3[1] * x + m3[4] * y + m3[7] * z,
	  			m3[2] * x + m3[5] * y + m3[8] * z,
	  		);
	},
	translated : (m3, v2)=>{
	   return M3.translate(M3.create(m3), v2);
	},
	scaled : (m3, v2)=>{
	   return M3.scale(M3.create(m3), v2);
	},
}
export const M4 = {
	create : (other)=>{
		const ret = other ? Float32Array.from(other) : new Float32Array(16);
		return ret;
	},
	identity : (toTransform)=>{
		const ret = toTransform || M4.create();
		ret[ 0]=1.; ret[ 1]=0.; ret[ 2]=0.; ret[ 3]=0.; 
		ret[ 4]=0.; ret[ 5]=1.; ret[ 6]=0.; ret[ 7]=0.; 
		ret[ 8]=0.; ret[ 9]=0.; ret[10]=1.; ret[11]=0.; 
		ret[12]=0.; ret[13]=0.; ret[14]=0.; ret[15]=1.; 
		return ret;
	},
	transpose : (m4)=>{
		const ret = M4.create(m4);

						ret[1] = m4[4]; ret[2] = m4[8]; ret[3] = m4[12];
		ret[4] = m4[1]; 				ret[6] = m4[9]; ret[7] = m4[13];
		ret[8] = m4[2]; ret[9] = m4[6]; 				ret[11]= m4[14];
		ret[12]= m4[3]; ret[13]= m4[7]; ret[14]= m4[11];
	    return ret;
	},
	translate : (m4, v3)=>{
	    m4[12] += m4[0] * v3.x + m4[4] * v3.y + m4[8] * v3.z;
	    m4[13] += m4[1] * v3.x + m4[5] * v3.y + m4[9] * v3.z;
	    m4[14] += m4[2] * v3.x + m4[6] * v3.y + m4[10] * v3.z;
	    m4[15] += m4[3] * v3.x + m4[7] * v3.y + m4[11] * v3.z;
	    return m4;
	},
	translated : (m4, v3)=>{
	   return M4.translate(M4.create(m4), v3);
	},
	lookat : (eye, up, point, toTransform)=>{
		const ret = toTransform || M4.create(),
			  Z   = V3.normalized(V3.sub(eye, point)),//forward
			  X   = V3.normalized(V3.cross(up, Z)),//right
			  Y   = V3.normalized(V3.cross(Z, X)),//up again
			  m30 = -V3.dot(X, eye),//-eye.x,//
			  m31 = -V3.dot(Y, eye),//-eye.y,//
			  m32 = -V3.dot(Z, eye);//-eye.z;//

		ret[ 0]=X.x; ret[ 1]=Y.x; ret[ 2]=Z.x; ret[ 3]=0.; 
		ret[ 4]=X.y; ret[ 5]=Y.y; ret[ 6]=Z.y; ret[ 7]=0.; 
		ret[ 8]=X.z; ret[ 9]=Y.z; ret[10]=Z.z; ret[11]=0.; 
		ret[12]=m30; ret[13]=m31; ret[14]=m32; ret[15]=1.; 
		return ret;
	},
	transform : (v, m4, w =1)=>{
		const {x = 0, y = 0, z = 0} = v;
	  	return V3.create(
	  			m4[0] * x + m4[4] * y + m4[7] * z + m4[10] * w,
	  			m4[1] * x + m4[5] * y + m4[8] * z + m4[11] * w,
	  			m4[2] * x + m4[6] * y + m4[9] * z + m4[12] * w,
	  		);
	},
	perspective : function (fovy, aspect, near, far) {
		/*
		  fovX 0    0    0
		  0    fovY 0    0
		  0    0    nf  -1
		  0    0    nf2  0
		*/
		const ret = M4.create(),
		      f = 1.0 / Math.tan(fovy / 2),
		      nf = 1 / (near - far);
		ret[0]  = f / aspect;
		ret[5]  = f;
		ret[10] = (far + near) * nf;
		ret[11] = -1;
		ret[14] = (2 * far * near) * nf;

		return ret;
	},
	orthographic : function (left, right, bottom, top, near, far) {
		const ret = M4.create();

		ret[0]  = 2.0 / (right-  left);
		ret[5]  = 2.0 / (top  -bottom);
		ret[10] = -2.0 / (far-  near);

		ret[12] = -(left+ right) / (right-  left);
		ret[13] = -(top +bottom) / (top  -bottom);
		ret[14] = -(far +  near) / (far  -  near);

		ret[15] = 1.0;

		return ret;
	},
	pretty: (m4)=>{
		return [
			[m4[ 0], m4[ 1], m4[ 2], m4[ 3]], 
			[m4[ 4], m4[ 5], m4[ 6], m4[ 7]], 
			[m4[ 8], m4[ 9], m4[10], m4[11]], 
			[m4[12], m4[13], m4[14], m4[15]], 
		].join("\n");
	}
}
export default {M4};