import glutils from "./glutils.js";
import {V3, V2} from "./math/vectors.js";
import {M3, M4} from "./math/matrices.js";
import sprite_vertex from "./shaders/sprite.vs";
import sprite_fragment from "./shaders/sprite.fs";

const TALLY = {
	wrld : 0,
	go : 0,
	rdr : 0
};
const engine = Object.create({
		running : false,
		latFrame : 0,
		sounds : {},
		inputState : {},
		init: function(options = {}){
			const 	world = this.createWorld(options),
					renderer = this.createRenderer(world, options),
					audio = options.audio ? new window.AudioContext() : null,
					ret = {world, renderer, audio};
			Object.assign(this, ret);
			const pickingCanvas = document.createElement("canvas");
			pickingCanvas.width = 11;
			pickingCanvas.height = 11;
			/*debug css :
			document.body.appendChild(pickingCanvas);
			pickingCanvas.setAttribute('style', 
				'left: 100px;\
				top: 100px;\
			    position: absolute;\
			    transform: scale(15)');//*/
			this.pickingCtx = pickingCanvas.getContext('2d');
			this.postRender = options.postRender;
			return ret;
		},
		start: function(options = {}){
			if(!this.running){
				requestAnimationFrame(this.update.bind(this));
				if(!options.resume)this.time = -1;
			}
			this.running = true;
		},
		stop: function(options){
			this.running = false;
		},
		getTime: function(){
			return this.time;
		},
		createGameobject: function(world, options={}){
			const 	ret = Object.create(GameObjectProto),
					{parent} = options;
			ret.init(world, options);
			parent && world.add(ret, parent);
			return ret;
		},
		createWorld: function(options){
			const ret = Object.create(WorldProto);
			ret.root = this.createGameobject(ret, {});
			ret.__id = TALLY.wrld++;
			worlds.push(ret);
			return ret;
		},
		updateInputs: function(renderer, inputState){
			Object.assign(this.inputState, inputState);
		},
		createRenderer: function(world, options){
			const ret = Object.create(RendererProto);
			ret.init(world, options);
			world.renderers.push(ret);
			ret.__id = TALLY.rdr++;

			ret.defaultCamera = {
				scale : 1,
				clearColor : [0.5, 0.5, 0.5, 1.0],

			};
			ret.cameras.push(ret.defaultCamera);

			return ret
		},
		createSprite: function(tex, options = {}) {
			let   {x =0, y=0, w = -1, h = -1, layer = 0, pivot, drag, click, interactive} = options;
			const   {renderer, world, pickingCtx} = this,
					pos = V2.create(x,y),
					angle = Math.random() * Math.PI * 2,
					mtx = M3.translated(M3.identity(), (pos || V2.create(0))),
					//mtx = M3.translate(M3.identity(), V2.create(100, 100)),
					shaders = {
						vertex : sprite_vertex,
						fragment : sprite_fragment
					},
					mtl = renderer.createMtl({shaders}),
					sprite = this.createGameobject(world, options);

			if(w === -1){
				if(h === -1){
					w= tex.width;
					h= tex.height;
				}else{
					w = h * tex.width / tex.height;
				}
			}else if(h === -1){
				h = w * tex.height / tex.width;
			}
			sprite.isSprite = true;
			sprite.layer = layer;
			sprite.interactive = interactive;
			sprite.drag = drag;
			sprite.click = click;
			world.add((time, inputState)=>{//checkMouseState
				if(sprite.interactive){
			    	const   //scaleX = (renderer.viewport.width / renderer.canvas.width),
			    			//scaleY = (renderer.viewport.height / renderer.canvas.height),
				    		{mousePos, startedDragging, clicked} = inputState,
				    		myPos = M3.getTranslation(mtx);
				    const 	localPos = mousePos && V2.sub(mousePos, myPos);
				    //TODO : don't cheese this out
				    if(localPos){
						if(sprite.dragging){
							//(sprite.name, localPos);
					    	sprite.mousePos = V2.create(localPos.x, localPos.y);
							return {hover : true};
					    }
				    	//localPos.y = renderer.renderHeight - localPos.y;
					   // localPos.x *=scaleX;
					   // localPos.y *=scaleY;
						//if(sprite.name === "rideauGauche")console.log(localPos);
						//if(sprite.name === "torso")console.log(localPos);
					    if(localPos.x > 0.0 && localPos.y > 0.0 && localPos.x < w && localPos.y < h){
					    	//console.log("the mouse is over", sprite.name, v);
					    	sprite.mousePos = V2.create(localPos.x, localPos.y);
					    	localPos.x *= (tex.imageBitmap.width / w) ;
					    	localPos.y = tex.imageBitmap.height - localPos.y * (tex.imageBitmap.height / h) ;
					    	pickingCtx.clearRect(0,0,11,11);
					    	pickingCtx.drawImage(tex.imageBitmap, localPos.x -5, localPos.y -5, 11, 11,0,0,11,11);
					    	const 	data = pickingCtx.getImageData(0,0,11,11).data;
							let alphaTotal = 0;
					    	for(let i = 3; i < data.length; i += 4)
					    		alphaTotal += data[i];
					    	alphaTotal /= data.length / 4;
					    	//console.log("alphaTotal", alphaTotal);
					    	return {hover : alphaTotal > 10};
					    }
					}
				    	
				}


			}, sprite);
				
			world.add(_=>{//render
						    const position = {
							    	type : "FLOAT",
							    	components : 2,
							    },
							   	textureCoord = {
							    	type : "FLOAT",
							    	components : 2,
							    };
							return {
						    	mtl,
						    	name: sprite.name,
						    	buffers : {position, textureCoord},
						    	mode : "TRIANGLE_STRIP",
						    	length : 4,
						    	layer
						    };
						}, sprite);
			sprite.setSize = (_w, _h, _px = 0, _py = 0)=>{
				//if(_w !== w ||_h !== h){

		    	mtl.attributes.position.set(Float32Array.from([
				    -_px,	  -_py,
				     _w-_px,  -_py,
				    -_px,	  _h-_py,
				     _w-_px,  _h-_py,
				  ]));
				h = _h, w = _w;
				// }
			}
			sprite.setPivot = (_x, _y)=>sprite.setSize(w, h, _x, _y)
			let _w = w, _h = h;
			w = 0; h = 0;
			sprite.setSize(_w, _h);
			sprite.setPos = (pos)=>{
				M3.setTranslation(mtx, pos);
			}
			sprite.setRotation = (pos)=>{
				M3.setRotation(mtx, pos);
			}
			sprite.getPos = ()=>{
				return M3.getTranslation(mtx);
			}
			sprite.setTexture = (_tex)=>{
				mtl.uniforms.tex && mtl.uniforms.tex.set(_tex);
				tex = _tex;
			}
			sprite.setTexCoords = (x, y, w, h)=>{
				const tW = tex.width,
					  tH = tex.height,
					  coords = [
				    x/tW,  (h+y)/tH,
				    (w+x)/tW,  (h+y)/tH,
				    x/tW,  y/tH,
				    (w+x)/tW,  y/tH,
				  ];
				console.log({coords})
		    	mtl.attributes.textureCoord.set(Float32Array.from(coords));
			}
			sprite.setTexCoords(0,0,tex.width, tex.height);
			mtl.uniforms.tex && mtl.uniforms.tex.set(tex);
			mtl.uniforms.modelMatrix && mtl.uniforms.modelMatrix.set(mtx);
			return sprite
		},
		stopSound: function({source, gain, tracker}, options = {}){
			const 	{audio, world} = this;
			if(!audio)
				return Promise.resolved(console.error("can't play sound : audio context was not inited"));
			const 	{
						fade = 1.0,
						onEnd
					} = options,
					now = audio.currentTime,
					destination = audio.destination;
			if(fade){
				gain.gain.linearRampToValueAtTime(0.0, now + fade);
				source.stop(now + fade);
			}else{
				gain.disconnect(destination)
				world.remove(tracker);
			}

		},
		playSound: function(name, options = {}){//TODO: should be gameObjects
			const 	{audio, world} = this,
					buffer = this.sounds[name];
			if(!audio)
				return Promise.resolve(console.error("can't play sound : audio context was not inited"));
			if(!buffer)
				return Promise.resolve(console.error("can't play sound : unknown asset"));
			const 	{
						fade = 0.5,
						offset = 0,
						volume = 1.0,
						delay = 0,
						loop = false,
						duration,
						onProgress,
						onEnd
					} = options,
					now = audio.currentTime,
					source = audio.createBufferSource(),
					destination = audio.destination,
					gain = audio.createGain(),
					tracker = world.add(time=>{
						onProgress && onProgress({source, gain})
					}),
					promise = new Promise(( resolve, reject)=>{
						if(fade){
							gain.gain.setValueAtTime(0, now);
							gain.gain.linearRampToValueAtTime(volume, now + fade);
						}else{
							gain.gain.setValueAtTime(volume, now);
						}
						source.buffer = buffer;
						source.loop = loop;

						source.onended = ()=>{
							gain.disconnect(destination)
							world.remove(tracker);
							onEnd && onEnd({source, gain});
							resolve(true);
						};
						source.connect(gain);
						gain.connect(destination);
						source.start(delay, offset, duration);
						
					});
			Object.assign(promise, {source, gain, tracker});
			return promise;
		},
		loadAssets: function({images, sounds}){
			const 	{audio, renderer} = this,
					imgProms = [],
					sndProms = [];
			if(images){
				for(let name of images){
					if(!renderer.textures[name])
						imgProms.push(
							renderer.loadTexture("./data/" + name + ".png")
								.then(tex=>renderer.textures[name] = tex));
				}
			}
			if(audio && sounds){
				for(let name of sounds){
					if(!this.sounds[name])
						sndProms.push(
							loadAudio(audio, "./data/" + name + ".mp3")
								.then(buffer=>this.sounds[name] = buffer));
				}
			}
			return Promise.all([
						Promise.all(imgProms),
						Promise.all(sndProms)
				]).then(([textures, sounds])=>{
						//console.log(textures, sounds)
						return {textures : renderer.textures, 
								sounds : this.sounds};
					});
		},
		update: function(time){
			const inc = time - this.lastFrame;
			this.lastFrame = time;
			if(this.running){
				if(isNaN(this.time) || this.time == -1)
					this.time = 0;
				this.time += inc;
				requestAnimationFrame(this.update.bind(this));
				for(let world of worlds){
					world.hovered(null);
					const state = world.update(this.time, this.inputState);
					if(state && state.length){
						for(let renderer of world.renderers){
							renderer.render(world, state);
						}
						world.renderDirty = false;
					}
				}
			}
			let hoveredObj = this.world.hoveredObj,
				startedDragging = this.inputState.startedDragging,
				dragging = this.inputState.dragging,
				stoppedDragging = this.inputState.stoppedDragging,
				clicked = this.inputState.clicked;
			if(hoveredObj){
				//if(hoveredObj.name ==="torso") console.log("torso drag B:", hoveredObj.dragging);
				dragging = dragging && hoveredObj.drag
					 && hoveredObj.drag(startedDragging, stoppedDragging, dragging, hoveredObj.mousePos, this.inputState.mousePos, hoveredObj);
				hoveredObj.dragging |= dragging;
				hoveredObj.dragging &= hoveredObj.interactive && dragging;

				//f(hoveredObj.name ==="torso") console.log("torso drag A:", hoveredObj.dragging);

				hoveredObj.hover && hoveredObj.hover();;
				if(clicked && hoveredObj.click)
					hoveredObj.click(hoveredObj.mousePos, this.inputState.mousePos, hoveredObj);
			}
			this.postRender && this.postRender(hoveredObj, this.inputState);
			this.inputState.startedDragging = false;
			this.inputState.stoppedDragging = false;
			this.inputState.dragging = false;
			this.inputState.clicked = false;
		}
	});
const 	worlds = [],
		WorldProto = {
			renderers : [],
			root : null,
			components : [],
			objects : [],
			state : null,
			renderDirty : false,
			isWorld : true,
			hoveredObj : null,
			update : function(time, inputState){
				// TODO double buffer states
				// and move to worker / transferable
				return this.root.update(time, inputState, null, this);
			},
			add:function(object, parent, layer){
				const p = parent || this.root;
				if(! object.isGameObject){
					if(typeof object !== "function")
						return console.error("can only add gameObjects or functions as(components)");

					const   func = object,
							obj = engine.createGameobject(this, {});
					func.__go = object = obj;
					obj.update = func.bind(obj);
				}
				if(p && p.children){
					const idx = p.children.indexOf(object);
					if(idx !== -1)
						p.children.splice(idx, 1);
					if(!layer)layer = object.layer;
					if(layer > 0){
						p.children.splice(layer, 0, object);
					}else if(layer < 0){
						p.children.splice(p.children + layer, 0, object);
					}
					else{
						layer = p.children.push(object);
					}
					object.layer = layer;
				}
				object.parent = p;
				object.world = this;
				return object;
			},
			hovered:function(sprite){
				const hoveredObj = this.hoveredObj;
				if(!sprite || ! hoveredObj || (sprite.dragging && !hoveredObj.dragging) || hoveredObj.layer < sprite.layer){
					if(hoveredObj && hoveredObj.dragging)
						return;
					this.hoveredObj = sprite;
				}
			},
			remove:function(object, parent){
				const 	p = parent || object.parent || this.root,
						go = object.isGameObject ? object : object.__go,
						array = (p && p.children) || [],
						idx = array.indexOf(go);
				if(idx === -1){
					return console.error(p, "was not the parent of", go);
				}
				array.splice(idx, 1);
			}
		},
		RendererProto = {
			isRenderer : true,
			domElement : null,
			cameras:[],
			textures :{},
			context:null,
			render: function(){},//null function
			init: function(world, options={}){
				const 	canvas = options.canvas || document.createElement('canvas'),
						defaultViewport = {x : 0, y : 0, width : canvas.width, height : canvas.height},
						{viewport = defaultViewport} = options,
						ctx = canvas && canvas.getContext('webgl');
				if(ctx){
					this.canvas = canvas;
					this.viewport = viewport;
					this.render = createRenderFunc(ctx, this);
					this.createMaterial = this.createMtl = createMtlFunc(ctx);
					this.loadTexture = loadTexture.bind(this, ctx);
				}else{
					console.error("couldn't retrieve context");
					this.render = _=>{}
					this.createMaterial = _=>null;
					this.loadTexture = _=>null;
				}
			}
			
		},
		GameObjectProto = {
			name : "",
			isGameObject : true,
			init : function(world, options = {}){
				this.children = [];
				this.world = world;
				this.__id = TALLY.go++;
				this.name = options.name || "GO::" + this.__id;
				this.live = true;
			},
			destroy : function(){
				this.live = false;
			},
			update : function(time, inputState, parent, world){
				let renders = [],
					removes = [],
					l = 1;
				if(!this.live) return renders.push({remove : true});
				for(let child of this.children){
					child.layer = l++;
					const state = child.update(time, inputState, this, world);
					if(state){
						const states = (isIterable(state))? state : [state];
						for(let st of states){
							if(st && st.mtl && st.buffers)
								renders.push(st);
							if(st.hover)
								world.hovered(this);
							if(st.remove)
								removes.push(child);
						}
					}
				}
				for(let child of removes){
					world.remove(child, this);
				}
				//return true if the engine should re-render
				return renders;
			}
		},
		parseNames = (shaderProgram, ctx, sources)=>{
			const 	attributes = {
					},
					uniforms = {
					};
			for( let source of sources){
				for( let {what, type, name} of nameFinder(source)){
					const obj = {name, type}
					switch(what){
						case "attribute":
						{
							attributes[name] = obj;
							obj.buffer = ctx.createBuffer();
							obj.location = ctx.getAttribLocation(shaderProgram, name);
							obj.set = (data)=>{

							    ctx.useProgram(shaderProgram);
							    ctx.bindBuffer(ctx.ARRAY_BUFFER, obj.buffer);
								ctx.bufferData(ctx.ARRAY_BUFFER, data,ctx.STATIC_DRAW);
							    ctx.useProgram(null);
							}
						}break;
						case "uniform":
						{
							uniforms[name] = obj;
							obj.location = ctx.getUniformLocation(shaderProgram, name);
							createUniformSetters(ctx, obj, type);
						}break;
					}
				}
			}
			return {attributes, uniforms};
		},
		loadAudio = function(ctx, url, onLoad){
			return fetch(url)
			  .then(response => response.arrayBuffer())
			  .then(buffer=>ctx.decodeAudioData(buffer))
			  .then(onLoad || (_=>_))
			  .catch(error=>console.log("error loading sound", url, error))
		},
		loadTexture = function(ctx, url, onLoad){
				const texture = ctx.createTexture();
				ctx.bindTexture(ctx.TEXTURE_2D, texture);

				const 	level = 0, border = 0,
						width = 1, height = 1,
						internalFormat = ctx.RGBA,
						srcFormat = ctx.RGBA,
						srcType = ctx.UNSIGNED_BYTE,
						pixel = Uint8Array.from([0, 0, 0, 255]);
				ctx.texImage2D(ctx.TEXTURE_2D, level, internalFormat,
				width, height, border, srcFormat, srcType,
				pixel);
				const end = imageBitmap=>{
					const {width, height} = imageBitmap;

					ctx.bindTexture(ctx.TEXTURE_2D, texture);
					ctx.texImage2D(ctx.TEXTURE_2D, level, internalFormat,
					srcFormat, srcType, imageBitmap);

					if (isPowerOf2(width) && isPowerOf2(height)) {
						ctx.generateMipmap(ctx.TEXTURE_2D);
					} else {
						ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_S, ctx.CLAMP_TO_EDGE);
						ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_T, ctx.CLAMP_TO_EDGE);
						ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MIN_FILTER, ctx.LINEAR);
					}
				  	return {
				  		texture,
				  		imageBitmap,
				  		width, height
				  	};
				};
				return fetch(url)
				  .then(response => response.blob())
				  .then(blob => createImageBitmap(blob))
				  .then(imageBitmap => {
					if ('requestIdleCallback' in window) {
					  return new Promise((resolve,reject)=>{
					  	requestIdleCallback(()=>resolve(end(imageBitmap)))
					  });
					} else {
					  return new Promise((resolve,reject)=>{
					  	resolve(end(imageBitmap));
					  });
					}
				  })
				  .then(onLoad || (_=>_))
				  .catch(error=>console.error("error loading texture", url, error));
		},
		createMtlFunc = ctx=>options=>{
			const 	{shaders} = options,
					{vertex, fragment} = shaders,
					shaderProgram = ctx && glutils.initShaderProgram(ctx, vertex, fragment),
					ret = {
					    program: shaderProgram,
					},
					info = parseNames(shaderProgram, ctx, [vertex, fragment]);
			Object.assign(ret, info);
			return ret;
		},
		createUniformSetters=(ctx,uniform,type)=>{
			let func, funcv;
			if(type === "sampler2D"){
				uniform.set = tex =>uniform.value = tex;
				uniform.upload = ctx=>{
					ctx.activeTexture(ctx.TEXTURE0);
					ctx.bindTexture(ctx.TEXTURE_2D, uniform.value.texture);
					ctx.uniform1i(uniform.location, 0);
				}
				return;
			}if(type.indexOf("mat") === 0){
				const num = type[3];
				func = "uniformMatrix"+num+"fv";
				uniform.set = uniform.setv = v=>uniform.value = v;
				uniform.upload = (ctx)=>ctx[func](uniform.location, false, uniform.value);
				return;
			}else{
				let num, t;
				if(type.indexOf("vec") === 0){
					num = type[3];
					t = "f";
				}else if(type.indexOf("ivec") === 0){
					num = type[4];
					t = "i";
				}else{
					num = 1;
					t = type === "int" ? "i" : "f";
				}
				funcv = "uniform"+num+t+"v";
				func  = "uniform"+num+t;
				if(func){
					uniform.set = (v0, v1, v2, v3)=>{
						uniform.value = v0;
						uniform.upload = (ctx)=>ctx[func](uniform.location, v0, v1, v2, v3);
					}
				}else
					uniform.set = _=>console.error("uniform", name, "can't be set with float values");

				if(funcv){
					uniform.setv = v=>{
						uniform.value = v;
						uniform.upload = (ctx)=>ctx[funcv](uniform.location, v);
					};
				}else
					uniform.setv = _=>console.error("uniform", name, "can't be set with composite values");

				uniform.set(0);
			}
				
		},
		createRenderFunc = (ctx, renderer)=>(world,state)=>{
			for(let cam of renderer.cameras){
				//console.log("render");
				const   {scale, clearColor} = cam,
						{canvas} = renderer,
						{x, y, width, height} = renderer.viewport,
						viewport = [x, y, width, height],
						viewportMatrix = M4.orthographic(
							x, x + width,
							y, y + height,
							0, state.length);

				//ctx.viewport(...viewport);
				ctx.viewport(0,0, canvas.width, canvas.height);
				//ctx.clearDepth(1.0);
				//ctx.enable(ctx.SAMPLE_COVERAGE);
				//ctx.sampleCoverage(1.0, false);
				//ctx.enable(ctx.DEPTH_TEST);
				//ctx.depthFunc(ctx.LEQUAL);
				if(clearColor){
					ctx.clearColor(...clearColor);
				}
				ctx.clear(ctx.COLOR_BUFFER_BIT);
				ctx.enable(ctx.BLEND);
				ctx.blendFunc(ctx.SRC_ALPHA, ctx.ONE_MINUS_SRC_ALPHA);
				let i = 0;
				for(let {mtl, buffers, mode, length, layer, name} of state){
					ctx.useProgram(mtl.program);
					for(let name in mtl.uniforms){
						const uniform = mtl.uniforms[name];
						if(name === "viewportMatrix"){
							uniform.setv(viewportMatrix);
						}else if(name === "max_layer"){
							uniform.set(1000 + state.length);
						}else if(name === "layer"){
							uniform.set(i);
						}

						uniform.location && uniform.upload(ctx);
					}
					for(let name in mtl.attributes){
						const   attrib = mtl.attributes[name],
								buffer = buffers[name];
						if(attrib && buffer){
							ctx.bindBuffer(ctx.ARRAY_BUFFER, attrib.buffer);
							ctx.enableVertexAttribArray(attrib.location);
						    ctx.vertexAttribPointer(
						        attrib.location,
						        buffer.components, // combien de components
						        ctx[buffer.type], // type
						        false, //pas de normalisation
						        0, // auto stride
						        0); // pas d'offset

						}

					}
	   				ctx.drawArrays(ctx[mode], 0, length);
					ctx.useProgram(null);
				}
				ctx.flush();
			}
		},
		isPowerOf2 = value=>(value & (value - 1)) == 0;


function* nameFinder(source){
	const reg = /^(attribute|uniform)\s+(\w+)\s+(\w+)\;.*$/gm;
	let match;

	while( (match = reg.exec(source)) !== null) {
		const [line, what, type, name] = match;
		yield {line, what, type, name};
	}
}
function isIterable(obj) {
  return obj && typeof obj[Symbol.iterator] === 'function';
}
export default engine;
